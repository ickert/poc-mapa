import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  PermissionsAndroid,
  Platform,
} from 'react-native';

import Geolocation from 'react-native-geolocation-service';
import styled from 'styled-components';

const Container = styled.View`
  flex: 1;
  padding-top: 100px;
  background-color: yellowgreen;
`;
const CustomText = styled.Text`
  color: rgba(0,0,0,.8);
  font-size: 30px;
`;

export default function App() {
  const [hasLocationPermission, setHasLocationPermission] = useState(false);
  const [userPosition, setUserPosition] = useState(false);
  const isIOS = Platform.OS === 'ios';

  async function verifyLocationPermission() {
    if (isIOS) {
      setHasLocationPermission(true);
      return;
    }
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('permissão concedida');
        setHasLocationPermission(true);
      } else {
        console.error('permissão negada');
        setHasLocationPermission(false);
      }
    } catch (err) {
      console.warn(err);
    }
  }

  useEffect(() => {
    verifyLocationPermission();

    if (hasLocationPermission) {
      Geolocation.watchPosition(
        position => {
          setUserPosition({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });

          console.log(`evento recebido`);
        },
        error => {
          console.log(error.code, error.message);
        },
      );
    }
  }, [hasLocationPermission]);

  return (
    <Container>
      <CustomText>Recebendo mudanca:</CustomText>
      <CustomText>Latitude: {userPosition.latitude}</CustomText>
      <CustomText>Longitude: {userPosition.longitude}</CustomText>
    </Container>
  );
}
